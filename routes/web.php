<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\FileUploadController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/fileUp')->group( function (){
    Route::get('/',[FileUploadController::class,'index'])->name('fileIndex');
    Route::get('/create',[FileUploadController::class,'create'])->name('fileCreate');
    Route::post('/',[FileUploadController::class,'store'])->name('fileStore');
    Route::get('/{file}',[FileUploadController::class,'show'])->name('fileShow');
    Route::get('/{file}/edit',[FileUploadController::class,'edit'])->name('fileEdit');
    Route::post('/{file}/up',[FileUploadController::class,'update'])->name('fileUpdate')->middleware('can:update,file');
    Route::post('/{file}/delete',[FileUploadController::class,'destroy'])->name('fileDel');
});
