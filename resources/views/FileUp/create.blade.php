@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    <form action="{{ route('fileStore') }}" method="post" enctype="multipart/form-data">
                        <label for="newFile"> Upload new file!</label>
                        <input type="file" id="newFile" name="newFile" >
                        @csrf
                        <input type="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
