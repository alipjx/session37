@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <a href="{{ route('fileCreate') }}"> Upload new file</a>
                        <div class="container">
                            @foreach ($files as $file)
                                <p>{{ $file->original_name }} created by {{ $file->user_name->name }}</p>
                            @endforeach
                        </div>

                        {{ $files->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
