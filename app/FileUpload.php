<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileUpload extends Model
{
    protected $table='files';
    protected $fillable=['original_name','name','created_at','updated_at'];

    public function user_name()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
