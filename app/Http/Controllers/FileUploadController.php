<?php

namespace App\Http\Controllers;

use App\FileUpload;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    public function index()
    {
        $files = FileUpload::query()->paginate('10');
        return view('fileUp.index',[ 'files' => $files ]);
    }

    public function create()
    {
        return view('fileUp.create');
    }
    public function store()
    {
        \request()->file('newFile')->store('files');
        $file = new FileUpload();
        $file->user_id = auth()->id();
        $file->original_name = \request()->file('newFile')->getClientOriginalName();
        $file->name = \request()->file('newFile')->hashName();
        $file->save();

        return $this->index();

    }

    public function show(FileUpload $file)
    {
        return view('fileUp.show',['file' => $file]);
    }
    public function edit()
    {

    }
    public function update()
    {

    }

    public function destroy()
    {

    }
}
